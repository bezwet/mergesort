package main;

public class Main {
    public static void main(String[] args) {
        MergeSort mergeSort = new MergeSort();
        int[] array = {8, 4, 2, 3, 10, 11, 5};
        print(array);
        mergeSort.sort(array);
        print(array);
    }


    public static void print(int[] array) {
        for (int j : array) {
            System.out.print(j + ",");
        }
        System.out.println();
    }
}